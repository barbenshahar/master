#
#   this script was written by Bar Ben Shahar
#   to run this script in the terminal: python {scripts_full_path} -u {tgz_url}
#   the script downloads tar file, extracts dcm files from it,
#   organizes them in the right hierarchy, and explores the data in order to answer on specific questions.
#   the script export all the new files and folders to the user's current working directory of a process.

import os
import shutil
import tarfile
import argparse
import pandas as pd
import urllib.request
from pydicom import dcmread


def download_tar(url, dicoms_path):
    """
    this function downloads the tar from the url (also checks if it is really a tar file),
    and extracts the dcm files into new folder in the user's current working directory of a process.
    """
    file_path = urllib.request.urlretrieve(str(url), f'{os.getcwd()}/original_file.tgz')
    if str(file_path[0]).split('.')[-1].lower() != 'tgz':
        return -1

    file = tarfile.open(file_path[0], 'r')

    for item in file:
        file.extract(item, dicoms_path)


def organize_dicoms(dst, dicoms_path):
    """
    this function extracts the relevant data from the dcm files
    and exports them in an organized hierarchy which is - patient_name/study_ID/series_ID
    the new folders will appear in the user's current working directory of a process.
    """
    for filename in os.listdir(dicoms_path):
        ds = dcmread(f'{dicoms_path}/{filename}')

        patient_name = str(ds['PatientName']).split("PN: ")[1][1:-1]
        if not os.path.isdir(f'{dst}/{patient_name}'):
            os.mkdir(f'{dst}/{patient_name}')

        study_ID = str(ds['StudyInstanceUID'])
        study_ID = study_ID.split("UI: ")[1]
        if not os.path.isdir(
                f'{dst}/{patient_name}/{study_ID}'):
            os.mkdir(f'{dst}/{patient_name}/{study_ID}')

        series_ID = str(ds['SeriesInstanceUID'])
        series_ID = series_ID.split("UI: ")[1]
        if not os.path.isdir(f'{dst}/{patient_name}/{study_ID}/{series_ID}'):
            os.mkdir(f'{dst}/{patient_name}/{study_ID}/{series_ID}')

        shutil.copy(f'{dicoms_path}/{filename}', f'{dst}/{patient_name}/{study_ID}/{series_ID}')


def patients_list(dicoms_path):
    """
    this function takes a directory full of dcm files and returns a dictionary with the patients names, sex and age
    this function also exports a csv files with the data.
    :param: dicoms_path: the directory of the dcm files
    :return patients details in a Data Frame (also export to CSV file
    """
    patients = {}
    for subdir, dirs, files in os.walk(dicoms_path):
        for file in files:
            ds = dcmread(os.path.join(subdir, file))
            name = str(ds['PatientName']).split("PN: ")[1][1:-1]
            age = str(ds['PatientAge']).split("AS: ")[1][2:-2]
            sex = str(ds['PatientSex']).split("CS: ")[1][1:-1]
            if name not in patients.keys():
                patients[name] = {'age': age, 'sex': sex}

    df_patiens = pd.DataFrame(patients)
    df_patiens.to_csv(f'{os.getcwd()}/list_of_patients.csv')
    return df_patiens


def average_scan(dicoms_path):
    """
    this function takes a directory full of dcm files, and returns and prints the average time of a CT scan exposure.
    :param: dicoms_path: the directory of the dcm files
    :return average time of a CT scan
    """
    scan_exposure = {}
    series_time = {}
    for subdir, dirs, files in os.walk(dicoms_path):
        for file in files:
            ds = dcmread(os.path.join(subdir, file))
            image_exposure = str(ds['ExposureTime'])
            image_exposure = image_exposure.split("IS: ")[1]
            image_exposure = image_exposure[1:-1]
            series_id = str(ds['SeriesInstanceUID']).split("UI: ")[1]
            if series_id not in scan_exposure:
                scan_exposure[series_id] = int(image_exposure)
            else:
                scan_exposure[series_id] += int(image_exposure)

    avg_exposure = pd.Series([scan_exposure[k] for k in scan_exposure]).mean()
    avg_exposure_sec = avg_exposure / 1000
    return f"the average of scan exposure is {avg_exposure} milliseconds, which are {avg_exposure_sec} seconds."


def hospitals(dicoms_path):
    """
    this function takes a directory full of dcm files, and returns and prints the number of hospitals the data come from
    :param dicoms_path: the directory of the dcm files
    :return: number of different hospitals
    """
    hospitals_list = []
    for subdir, dirs, files in os.walk(dicoms_path):
        for file in files:
            ds = dcmread(os.path.join(subdir, file))
            institution = str(ds['InstitutionName'])
            institution = institution.split("LO: ")[1]
            if institution not in hospitals_list:
                hospitals_list.append(institution)

    hospitals_count = len(hospitals_list)
    return f'There are: {hospitals_count} hospitals: {hospitals_list}'


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', type=str, help='enter the url of the .tgz file here')
    args = parser.parse_args()

    dicoms_path = f'{os.getcwd()}/dcm_files'
    if not os.path.isdir(dicoms_path):
        os.mkdir(dicoms_path)
    dst = f'{os.getcwd()}/organized_files_path'
    if not os.path.isdir(dst):
        os.mkdir(dst)

    download_tar(args.url, dicoms_path)
    organize_dicoms(dst, dicoms_path)
    print(patients_list(dicoms_path))
    print(average_scan(dicoms_path))
    print(hospitals(dicoms_path))
