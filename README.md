This script was written by Bar Ben Shahar

To run this script in the terminal: python {scripts_full_path} -u {tgz_url}

The script downloads tar file, extracts dcm files from it,

Organizes them in the right hierarchy, and explores the data in order to answer on specific questions.

The script export all the new files and folders to the user's current working directory of a process.